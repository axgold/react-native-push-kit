# react-native-push-kit

A PushKit module for React Native.

## Install
### iOS
1. `npm install react-native-push-kit@https://axgold@bitbucket.org/axgold/react-native-push-kit.git --save`
1. In Xcode, in the project navigator, right click `Libraries` > `Add Files to [project name]`
1. Go to `node_modules` > `react-native-push-kit` and add `RNPushKit.xcodeproj`
1. In Xcode, in the project navigator, select your project. Add `libRNPushKit.a` to your project's `Build Phases` > `Link Binary With Libraries`
1. Run your project (`Cmd+R`)

## Usage
```
var PushKit = require('react-native-push-kit');
PushKit.voipRegistration();

PushKit.addEventListener('register', function(userInfo) {
  console.log('You are registered for PushKit and the device token is: ' + userInfo.deviceToken);
});

PushKit.addEventListener('notification', function(notification) {
    console.log('You have received a new notification: ' + notification._alert);
});
```