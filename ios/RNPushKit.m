#import "RNPushKit.h"

#import "RCTBridge.h"
#import "RCTConvert.h"
#import "RCTEventDispatcher.h"
#import "RCTUtils.h"


// bridge dispatcher consts
static NSString *const RCTRemoteNotificationsRegistered = @"RemoteNotificationsRegistered";
static NSString *const RCTRemoteNotificationReceived = @"RemoteNotificationReceived";
static NSString *const RCTLocalNotificationActionReceived = @"LocalNotificationActionReceived";

// Notification center consts
static NSString *const LocalNotificationActionReceived = @"LocalNotificationActionReceived";

PKPushRegistry *voipRegistry;

// Enums
@implementation RCTConvert (UserNotificationActionContext)
RCT_ENUM_CONVERTER(UIUserNotificationActionContext,
                   (@{ @"userNotificationActionContextDefault" : @(UIUserNotificationActionContextDefault),
                       @"userNotificationActionContextMinimal" : @(UIUserNotificationActionContextMinimal)}),
                   UIUserNotificationActionContextDefault, integerValue)
@end

@implementation RCTConvert (UserNotificationActivationMode)
RCT_ENUM_CONVERTER(UIUserNotificationActivationMode,
                   (@{ @"userNotificationActivationModeForeground" : @(UIUserNotificationActivationModeForeground),
                       @"userNotificationActivationModeBackground" : @(UIUserNotificationActivationModeBackground)}),
                   UIUserNotificationActivationModeForeground, integerValue)
@end

@implementation RCTConvert (UILocalNotification)
+ (UILocalNotification *)UILocalNotification:(id)json {
    NSDictionary<NSString *, id> *details = [self NSDictionary:json];
    UILocalNotification *notification = [UILocalNotification new];
    notification.alertBody = [RCTConvert NSString:details[@"_alert"]];
    notification.category = [RCTConvert NSString:details[@"_category"]];
    return notification;
}
@end

@implementation RCTConvert (UIUserNotificationAction)
+ (UIUserNotificationAction *)UIMutableUserNotificationAction:(id)json {
    NSDictionary<NSString *, id> *details = [self NSDictionary:json];
    UIMutableUserNotificationAction *notificationAction = [UIMutableUserNotificationAction new];
    notificationAction.identifier = [RCTConvert NSString:details[@"_identifier"]];
    notificationAction.title = [RCTConvert NSString:details[@"_title"]];
    notificationAction.activationMode = [RCTConvert UIUserNotificationActivationMode:details[@"_activationMode"]];
    notificationAction.destructive = [RCTConvert BOOL:details[@"_destructive"]];
    notificationAction.authenticationRequired = [RCTConvert BOOL:details[@"_authenticationRequired"]];
    return notificationAction;
}
@end

@implementation RCTConvert (UIUserNotificationCategory)
+ (UIUserNotificationCategory *)UIMutableUserNotificationCategory:(id)json {
    NSDictionary<NSString *, id> *details = [self NSDictionary:json];
    UIMutableUserNotificationCategory *userNotificationCategory = [UIMutableUserNotificationCategory new];
    userNotificationCategory.identifier = [RCTConvert NSString:details[@"_identifier"]];
    NSMutableArray<UIMutableUserNotificationAction*> *actionsArray = [NSMutableArray new];
    
    for (NSDictionary *actionJson in [RCTConvert NSArray:details[@"_actions"]]) {
        [actionsArray addObject:[self UIMutableUserNotificationAction:actionJson]];
    }
    
    UIUserNotificationActionContext context = [RCTConvert UIUserNotificationActionContext:details[@"_context"]];
    [userNotificationCategory setActions:actionsArray forContext:context]; 
    return userNotificationCategory;
}
@end

@implementation RCTConvert (UIMutableUserNotificationCategoryArray)
+ (NSArray<UIMutableUserNotificationCategory *> *)UIMutableUserNotificationCategoryArray:(id)json {
    NSMutableArray<UIMutableUserNotificationCategory*> *categoryArray = [NSMutableArray<UIMutableUserNotificationCategory*> new];
    for (NSDictionary *categoryJson in json) {
        [categoryArray addObject:[self UIMutableUserNotificationCategory:categoryJson]];
    }
    return categoryArray;
}
@end


@implementation RNPushKit

RCT_EXPORT_MODULE()

@synthesize bridge = _bridge;

- (id)init {
    if (self = [super init]) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(handleLocalNotificationActionReceived:)
                                                     name:LocalNotificationActionReceived
                                                   object:nil];
        return self;
    } else {
        return nil;
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (NSDictionary *)constantsToExport {
    return @{ // UIUserNotificationActionContext
              @"userNotificationActionContextDefault": @(UIUserNotificationActionContextDefault),
              @"userNotificationActionContextMinimal": @(UIUserNotificationActionContextMinimal),
              // UIUserNotificationActivationMode
              @"userNotificationActivationModeForeground": @(UIUserNotificationActivationModeForeground),
              @"userNotificationActivationModeBackground": @(UIUserNotificationActivationModeBackground)};
};

RCT_EXPORT_METHOD(voipRegistration) {
    voipRegistry = [[PKPushRegistry alloc] initWithQueue:dispatch_get_main_queue()];
    voipRegistry.delegate = self;
    voipRegistry.desiredPushTypes = [NSSet setWithObject:PKPushTypeVoIP];
}

RCT_EXPORT_METHOD(presentLocalNotification:(UILocalNotification *)notification) {
    [RCTSharedApplication() presentLocalNotificationNow:notification];
}

RCT_EXPORT_METHOD(registerCategories:(NSArray<UIMutableUserNotificationCategory*> *)categories) {
    UIUserNotificationSettings *settings;
    UIUserNotificationType types = settings.types;
    
    // Registering UIUserNotificationSettings more than once results in previous settings being overwritten
    [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:types categories:[NSSet setWithArray:categories]]];
}

// PKPushRegistryDelegate delegate methods
- (void)pushRegistry:(PKPushRegistry *)registry didUpdatePushCredentials:(PKPushCredentials *)credentials forType:(NSString *)type {
    NSMutableString *hexString = [NSMutableString string];
    NSUInteger deviceTokenLength = credentials.token.length;
    const unsigned char *bytes = credentials.token.bytes;
    
    for (NSUInteger i = 0; i < deviceTokenLength; i++) {
        [hexString appendFormat:@"%02x", bytes[i]];
    }
    NSDictionary<NSString *, id> *userInfo = @{ @"deviceToken" : [hexString copy] };
    
    [_bridge.eventDispatcher sendDeviceEventWithName:RCTRemoteNotificationsRegistered
                                                body:userInfo];
}

- (void)pushRegistry:(PKPushRegistry *)registry didReceiveIncomingPushWithPayload:(PKPushPayload *)payload forType:(NSString *)type {
    if (![type isEqualToString:PKPushTypeVoIP]) {
        return;
    }
    
    [_bridge.eventDispatcher sendDeviceEventWithName:RCTRemoteNotificationReceived
                                                body:payload.dictionaryPayload];
}

- (void)handleLocalNotificationActionReceived:(NSNotification *)notification {
    [_bridge.eventDispatcher sendAppEventWithName:RCTLocalNotificationActionReceived
                                             body:notification.userInfo];
}

+ (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forLocalNotification:(UILocalNotification *)notification withResponseInfo:(NSDictionary *)responseInfo completionHandler:(void (^)())completionHandler {
    
    NSMutableDictionary *userInfo = @{@"identifier": identifier,
                                      @"category": notification.category};
    
    [[NSNotificationCenter defaultCenter] postNotificationName:LocalNotificationActionReceived
                                                        object:self
                                                      userInfo:userInfo];
    if (completionHandler != nil) {
        completionHandler();
    }
}

@end