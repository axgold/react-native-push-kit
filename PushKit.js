'use strict';

var React = require('react-native');
var invariant = require('invariant');
var {
    DeviceEventEmitter,
    NativeAppEventEmitter
} = React;

var RNPushKit = require('react-native').NativeModules.RNPushKit;

var _notifHandlers = new Map();

var REMOTE_NOTIFICATIONS_REGISTERED_EVENT = 'RemoteNotificationsRegistered';
var REMOTE_NOTIFICATION_RECEIVED_EVENT = 'RemoteNotificationReceived';
var LOCAL_NOTIFICATON_ACTION_RECEIVED_EVENT = 'LocalNotificationActionReceived';

export class PushKit {
    
    // Constants
    //
    // UIUserNotificationActionContext
    static default = RNPushKit.userNotificationActionContextDefault;
    static minimal = RNPushKit.userNotificationActionContextMinimal;
    // UIUserNotificationActivationMode
    static foreground = RNPushKit.userNotificationActivationModeForeground;
    static background = RNPushKit.userNotificationActivationModeBackground;
    
    static localNotification(msg) {
        RNPushKit.localNotification(msg);
    }

    static voipRegistration() {
        RNPushKit.voipRegistration();
    }

    static addEventListener(type: string, handler: Function) {
        invariant(
            type === 'register' || type === 'notification', 'PushKit only supports `register` and `notification` events'
            );

        var listener;
        if (type === 'register') {
            listener = DeviceEventEmitter.addListener(
                REMOTE_NOTIFICATIONS_REGISTERED_EVENT,
                (token) => {
                    handler(token);
                }
                );
        }
        else if (type === 'notification') {
            listener = DeviceEventEmitter.addListener(
                REMOTE_NOTIFICATION_RECEIVED_EVENT,
                (data) => {
                    // Using the same approach as PushNotificationIOS to support default APNS msg bodies.
                    // Custom templates can be created / used to support specific data needs.
                    handler(new PushKit(data));
                }
                );
        }

        _notifHandlers.set(handler, listener);
    }
    
    static addLocalNotificationActionEventListener(identifier, handler) {
        var listener;
        listener = NativeAppEventEmitter.addListener(
            LOCAL_NOTIFICATON_ACTION_RECEIVED_EVENT,
            (data) => {
                if (data.identifier === identifier) {
                    handler(data);
                }
            }
        )
    }

    static presentLocalNotification(details: Object) {
        RNPushKit.presentLocalNotification(details);
    }
    
    static registerCategories(categories) {
        RNPushKit.registerCategories(categories);
    }

    static removeEventListener(type: string, handler: Function) {
        invariant(
            type === 'register' || type === 'notification', 'PushKit only supports `register` and `notification` events'
            );
            
        var listener = _notifHandlers.get(handler);
        if (!listener) {
            return;
        }
        listener.remove();
        _notifHandlers.delete(handler);
    }

    constructor(nativeNotif: Object) {
        this._data = {};
        
        // Using the same approach as PushNotificationIOS to support default APNS msg bodies.
        // Custom templates can be created / used to support specific data needs.

        // Extract data from Apple's `aps` dict as defined:
        // https://developer.apple.com/library/ios/documentation/NetworkingInternet/Conceptual/RemoteNotificationsPG/Chapters/ApplePushService.html

        Object.keys(nativeNotif).forEach((notifKey) => {
            var notifVal = nativeNotif[notifKey];
            if (notifKey === 'aps') {
                this._alert = notifVal.alert;
                this._sound = notifVal.sound;
                this._badgeCount = notifVal.badge;
            } else {
                this._data[notifKey] = notifVal;
            }
        });
    }
}

module.exports = PushKit;