#import <UIKit/UIKit.h>
#import <PushKit/PushKit.h>

#import "RCTBridgeModule.h"

@interface RNPushKit : NSObject <RCTBridgeModule, PKPushRegistryDelegate>

+ (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forLocalNotification:(UILocalNotification *)notification withResponseInfo:(NSDictionary *)responseInfo completionHandler:(void (^)())completionHandler;

@end