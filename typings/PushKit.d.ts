declare class PushKit {
    static localNotification(msg: string): void;
    static voipRegistration(): void;
    static addEventListener(type: string, handler: Function): void;
    static presentLocalNotification(details: Object): void;
    static removeEventListener(type: string, handler: Function): void;
    constructor(nativeNotif: Object);
}

export = PushKit;
